/*
 * Programmed by Muhammad Ahsan
 * muhammad.ahsan@gmail.com
 * Research Intern
 * Yahoo! Research Barcelona
 * Spain
 */
package Tokenizer;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;
import javax.swing.JFileChooser;

/**
 *
 * @author Ahsan
 */
public class Tokenizer {

    /**
     * @param args the command line arguments
     */
    static String GetFileLocation() {

        // TODO code application logic here
        String             Path = null;
        final JFileChooser fc   = new JFileChooser();

        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        System.out.println("Please input file :");

        Boolean flag = false;

        do {
            int returnVal = fc.showOpenDialog(fc);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                Path = fc.getSelectedFile().toString();

                // This is where a real application would open the file.
                System.out.println(Path);
                flag = true;
            } else {
                System.out.println("Program is going exit.");
                System.exit(0);    // calling the method is a must
            }
        } while (flag != true);

        return Path;
    }

    static void SentenceToken(String aFileName) throws IOException {
        Path path = Paths.get(aFileName);

        try (Scanner scanner = new Scanner(path)) {
            while (scanner.hasNextLine()) {

                // process each line in some way
                System.out.println(scanner.nextLine());
            }
        }
    }

    public static void WordToken(String aFileName) throws IOException {
        ArrayList<String> tokens = new ArrayList<>();
        Path              path   = Paths.get(aFileName);

        try (Scanner scanner = new Scanner(path)) {
            while (scanner.hasNextLine()) {

                // process each line in some way
                // System.out.println(scanner.nextLine());
                String  test     = scanner.nextLine();
                Scanner tokenize = new Scanner(test);

                while (tokenize.hasNext()) {
                    tokens.add(tokenize.next());
                }
            }

            System.out.println(tokens);
        }
    }

    public static void main(String[] args) throws IOException {

        // TODO code application logic here
        String aFileName = GetFileLocation();
        WordToken(aFileName);
    }
}
